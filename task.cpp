// Quest.cpp

#include "Task.h" // Include the header file for the Tasks class
#include <iostream> // Include the standard input/output stream library

// Constructor for the Tasks class
Tasks::Tasks(std::string name, std::string description, std::string objectives, bool completed) : name(name), description(description), objectives(objectives), completed(completed) {}

// Function to get the name of the task
std::string Tasks::GetName() {
    return name; // Return the name of the task
}

// Function to get the description of the task
std::string Tasks::GetDescription() {
    return description; // Return the description of the task
}

// Function to get the objectives of the task
std::string Tasks::GetObjectives() {
    return objectives; // Return the objectives of the task
}

// Function to check if the task is completed
bool Tasks::IsCompleted() {
    return completed; // Return the completion status of the task
}

// Function to mark the task as completed
void Tasks::Complete() {
    completed = true; // Set the completion status of the task to true
}

// Constructor for the TaskLog class, inheriting from the Tasks class
TaskLog::TaskLog(std::string name, std::string description, std::string objectives, bool completed) : Tasks(name, description, objectives, completed) {};

// Function to add a task to the task log
void TaskLog::addtask(Tasks& task) {
    tasks.push_back(task); // Add the task to the list of tasks in the task log
}

// Function to mark a task as completed in the task log
void TaskLog::taskcomplete(Tasks& taskToComplete) {
    // Iterate through the list of tasks in the task log
    for (auto& task : tasks) {
        // Check if the name of the task matches the name of the task to complete
        if (task.GetName() == taskToComplete.GetName()) {
            // Mark the task as completed
            task.Complete();
            // Print a message indicating that the task has been completed
            std::cout << "Task '" << taskToComplete.GetName() << "' completed!" << std::endl;
            return; // Exit the function
        }
    }
    // If the task is not found in the task log, print a message indicating that it was not found
    std::cout << "Task not found." << std::endl;
}
