#pragma once 
// Prevents multiple inclusion of this header file

#include <iostream> // Input/output stream library
#include <string> // String manipulation library
#include "Room.h" // Includes the header file for "Room.h"

// Declaration of the Area class

class Area {
private:
    std::map<std::string, Room*> rooms; // Map to store rooms with room names as keys and Room pointers as values
    Room* startingRoom; // Pointer to the starting room of the Area
public:
    void AddRoom(std::string name, Room* room); // Add a room to the area
    Room* GetRoom(std::string name); // Retrieve a room by its name from the area
    void ConnectRooms(std::string room1Name, std::string room2Name, std::string direction); // Connect two rooms in the area
    void LoadMapFromFile(std::string filename); // Load the map from a text file
    Room* GetStartingRoom(); // Get the starting room of the area
};
