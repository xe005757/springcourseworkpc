#pragma once 
// Prevents multiple inclusion of this header file

#include <iostream> // Input/output stream library
#include <string> // String manipulation library
#include <map> // Provides functionality for associative containers (map)
#include <vector> // Provides functionality for dynamic arrays (vectors)
#include <fstream> // File stream library
#include <sstream> // String stream library
#include "Item.h" // Includes the header file for the Item class

// Declaration of the Room class

class Room {
private:
    std::string description; // Description of the room
    std::map<std::string, Room*> exits; // Map to store exits, with room names as keys and Room pointers as values
    std::vector<Item> items; // Vector containing the room's inventory of items 
public:
    void PrintConnections(); // Prints the connections available from the room
    Room(std::string desc); // Constructor to initialize the room with a description
    void AddItem(Item& item); // Adds an item to the room's inventory
    std::string GetDescription(); // Returns the description of the room
    std::vector<Item> GetItems(); // Returns the items present in the room
    std::map<std::string, Room*>& GetExits(); // Returns the exits available from the room
    Room* GetExit(std::string direction); // Returns the room connected to a specified exit direction
    void connectExit(std::string direction, Room* room); // Connects an exit from the current room to a new room
};
