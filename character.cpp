#include <iostream> // Library for input and output operations
#include <string> // Library for string manipulation
#include <map> // Library for map container
#include <vector> // Library for working with dynamic arrays (vectors)
#include <fstream> // Library for file input and output operations
#include <sstream> // Library for string stream operations
#include "Item.h" // Header file for "Item.h"
#include "Room.h" // Header file for "Room.h"
#include "Character.h" // Header file for "Character.h"


// Character class


// The class representing characters in the game

Character::Character(std::string& name, int health) : name(name) {}
/*
Constructor for Character class, initializes member variables such as name and health with values passed as parameters
*/
void Character::PrintStats() { // Function to output the character's stats
    std::cout << "\nYou have a health of " << health << std::endl;
    // Outputs the character's stats
}
void Character::TakeDamage(int damage) { // Function to decrease the character's health
    health = health - damage; // Decreases the character's health
    if (health > 0) { // Checks if the character's health is greater than 0
        std::cout << "\nYou have taken " << damage << " damage. Your health is: " << health << std::endl;
        // Outputs a message indicating the damage taken and the current health
    }
    else {
        health = 0; // Sets health to 0
        std::cout << "\nYou died! Better luck next time." << std::endl; // Outputs a message indicating the character's death
        std::exit(0); // Exits the game
    }
}
void Character::IncreaseHealth(int healthgain) { // Function to increase the character's health
    health = health + healthgain; // Increases the character's health
    if (health > 100) { // Checks if the character's health exceeds 100
        std::cout << "\nYour health is: " << health << std::endl;
        // Outputs a message indicating the current health
    }
    else {
        health = 100; // Sets health to 100
        std::cout << "\nYou are at max health." << std::endl; // Outputs a message indicating max health
    }
}
void Character::AddToInventory(Item& newItem) { // Function to add an item to the character's inventory
    inventory.emplace_back(newItem); // Adds the new item
    std::cout << "\nAmount of items in inventory: " << inventory.size() << "\n" << std::endl;
    // Outputs the number of items in the inventory
}
std::string& Character::GetName() { // Function to get the character's name
    return name; // Returns the character's name
}
std::vector<Item>& Character::GetInventory() { // Function to get the character's inventory
    return inventory; // Returns the character's inventory
}

// Player class


// The class representing the player in the game

Player::Player(std::string name, int health) : Character(name, health) {}
/*
Constructor for Player class, which inherits from the Character class. Initializes base class attributes: name and health.
*/
Room* Player::GetLocation() { // Function to get the player's location
    return location; // Returns the player's location
}
void Player::SetLocation(Room* newLocation) { // Function to set the player's new location
    location = newLocation; // Sets newLocation to the player's current location
}
