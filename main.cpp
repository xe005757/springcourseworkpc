#include <iostream> //Allows for inputs and outputs
#include <string> //Allows for string manipulation
#include <map> //Include header file for map container
#include <vector> //Allows for working with dynamic arrays (vectors)
#include <fstream> //Allows for file input and output operatons
#include <sstream> //Allows for string stream operations
#include <random> // Provides facilities for generating random numbers
#include <chrono> // Provides utilities for measuring time
#include <thread> // Provides utilities for managing threads and sleeping


//Include the files for the game


#include "Item.h" //Includes the header file for "Item.h"
#include "Area.h" //Includes the header file for "Area.h"
#include "Character.h" //Includes the header file for "Character.h"
#include "Room.h" //Includes the header file for "Room.h"
#include "Task.h" //Includes the header file for "Tasks.h"



int main() {
    // Create an instance of the Area class
    Area gameWorld;
    // Load the game map from a text file
    gameWorld.LoadMapFromFile("game_map.txt");

    // Create a Player
    Player player("Alice", 100);
    // Set the player's starting room (can be modified)
    Room* currentRoom = gameWorld.GetRoom("startRoom");
    Room* room2 = gameWorld.GetRoom("Room2");
    Room* room3 = gameWorld.GetRoom("Room3");
    Room* room4 = gameWorld.GetRoom("Room4");
    Room* room5 = gameWorld.GetRoom("Room5");

    player.SetLocation(currentRoom);

    // Create items
    Item elixir("Elixir", "Adds 50 health");
    Item sword("Sword", "A sharp weapon for combat");
    Item potion("Potion", "Damages the player by 20 health");

    // Add items to rooms
    currentRoom->AddItem(elixir);
    room2->AddItem(sword);
    room3->AddItem(potion);
    room4->AddItem(potion);
    room5->AddItem(sword);

    // Create tasks
    Tasks task1("Sword_fighter", "Only the true warriors carry a sword at all times", "Collect a sword", false);
    Tasks task2("Room_4_good", "In Room 4 lies a wonderful potion", "Go to room 4", false);

    // Create a task log for the player
    TaskLog playerLog("Name", "Description", "Objectives", false);

    // Add tasks to the player's task log
    playerLog.addtask(task1);
    playerLog.addtask(task2);

    // Game loop
    while (true) {
        // Display current location
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;
        // Check if a task is completed based on the player's location
        if (player.GetLocation()->GetDescription() == "A dark cellar with cobwebs hanging from the ceiling.") {
            playerLog.taskcomplete(task2);
        }
        // Display items in the room
        std::cout << "Items in the room:" << std::endl;
        for (Item& item : player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }
        // Display available exits
        std::cout << "Exits available: ";
        for (auto& exit : player.GetLocation()->GetExits()) {
            std::cout << exit.first << " ";
        }
        std::cout << "| ";
        // Display player options
        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Quit" << std::endl;
        int choice;
        std::cin >> choice;
        if (choice == 1) {
            // Player looks around (no action required)
            std::cout << "You look around the room." << std::endl;
        }
        else if (choice == 2) {
            // Player interacts with an item in the room
            std::cout << "Enter the name of the item you want to interact with:";
            std::string itemName;
            std::cin >> itemName;
            for (Item& item : player.GetLocation()->GetItems()) {
                if (item.GetName() == itemName) {
                    item.Interact();
                    // Check if interacting with a specific item completes a task
                    if (itemName == "Potion") {
                        player.TakeDamage(20);
                    }
                    else if (itemName == "Sword") {
                        player.AddToInventory(sword);
                        playerLog.taskcomplete(task1);
                    }
                    else if (itemName == "Elixir") {
                        player.IncreaseHealth(50);
                    }
                    break;
                }
            }
        }
        else if (choice == 3) {
            // Player moves to another room
            std::cout << "Enter the direction (e.g., north, south): ";
            std::string direction;
            std::cin >> direction;
            Room* nextRoom = player.GetLocation()->GetExit(direction);
            if (nextRoom != nullptr) {
                player.SetLocation(nextRoom);
                std::cout << "You move to the next room." << std::endl;
                currentRoom = nextRoom;
            }
            else {
                std::cout << "You can't go that way." << std::endl;
            }
        }
        else if (choice == 4) {
            // Quit the game
            std::cout << "Goodbye!" << std::endl;
            break;
        }
        else {
            std::cout << "Invalid choice. Try again." << std::endl;
        }
    }
}
