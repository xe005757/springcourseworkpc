// Quest.h
#pragma once

#include <string> // String manipulation library
#include <vector> // Provides functionality for dynamic arrays (vectors)

// Declaration of the Tasks class
class Tasks {
private:
    std::string name; // Name of the task
    std::string description; // Description of the task
    std::string objectives; // Objectives of the task
    bool completed; // Completion status of the task

public:
    Tasks(std::string name, std::string description, std::string objectives, bool completed); // Constructor to initialize task attributes
    std::string GetName(); // Returns the name of the task
    std::string GetDescription(); // Returns the description of the task
    std::string GetObjectives(); // Returns the objectives of the task
    bool IsCompleted(); // Checks if the task is completed
    void Complete(); // Marks the task as completed
};

// Declaration of the TaskLog class, which inherits from the Tasks class
class TaskLog : public Tasks {
private:
    std::vector<Tasks> tasks; // Vector to store tasks in the task log

public:
    TaskLog(std::string name, std::string description, std::string objectives, bool completed); // Constructor to initialize task log
    void taskcomplete(Tasks& taskToComplete); // Marks a task as completed in the task log
    void addtask(Tasks& task); // Adds a task to the task log
};
