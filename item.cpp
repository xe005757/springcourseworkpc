#include <iostream> // Input/output stream library
#include <string> // String manipulation library
#include <map> // Library for associative containers (map)
#include <vector> // Library for dynamic arrays (vectors)
#include <fstream> // File stream library
#include <sstream> // String stream library
#include "Item.h" // Includes the header file for the Item class

// Item class implementation

// Constructor for the Item class
Item::Item(std::string name, std::string desc) : name(name), description(desc) {}

// Function to interact with the item
void Item::Interact() {
    // Output the name and description of the item
    std::cout << "Name: " << name << "\nDescription: " << description << std::endl;
}

// Function to get the name of the item
std::string Item::GetName() {
    return this->name; // Returns the name of the item
}

// Function to get the description of the item
std::string Item::GetDescription() {
    return this->description; // Returns the description of the item
}

// Overloaded equality operator to compare two Item objects
bool Item::operator==(Item& other) {
    // Check if the names of the items are equal
    if (this->name == other.name) {
        return true; // Returns true if names are equal
    }
    else {
        return false; // Returns false if names are not equal
    }
}
