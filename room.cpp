#include <iostream> // Allows input and output operations
#include <string> // Enables string manipulation
#include <map> // Provides the map container for managing exits
#include <vector> // Supports dynamic arrays (vectors) for items
#include <fstream> // Facilitates file input and output operations
#include <sstream> // Enables string stream operations
#include "Room.h" // Includes the header file for the Room class
#include "Item.h" // Includes the header file for the Item class

// Room class implementation

Room::Room(std::string desc) : description(desc) {}; // Constructor for Room class, initializes description with provided description
void Room::PrintConnections() { // Prints the connections of the room
    std::cout << "\nRoom: " << description << " & ";
    for (auto& exit : exits) { // Iterates through available exits
        std::cout << "Exit: " << exit.first << ", Connected Room: " << exit.second->GetDescription() << "\n" << std::endl;
        // Outputs exit direction and description of connected room
    }
}
void Room::AddItem(Item& item) { // Adds an item to the room
    items.emplace_back(item); // Adds the item to the room's items
};
std::string Room::GetDescription() { // Returns the description of the room
    return description; // Returns the description
}
std::vector<Item> Room::GetItems() { // Returns the items present in the room
    return items; // Returns the items
}
std::map<std::string, Room*>& Room::GetExits() { // Returns the exits available from the room
    return exits; // Returns the exits
}
Room* Room::GetExit(std::string direction) { // Retrieves the room connected to a specific exit direction
    auto it = exits.find(direction); // Searches for the exit in exits map based on given direction
    std::cout << "\n" << direction; // Outputs the direction being searched for
    if (it != exits.end()) { // Checks if the exit was found
        return it->second; // Returns a pointer to the connected room
    }
    else {
        std::cout << "\nExit not found for direction " << direction << std::endl; // Outputs if the exit wasn't found
        return nullptr; // Returns a null pointer
    }
}
void Room::connectExit(std::string direction, Room* room) { // Connects the room to an exit
    exits[direction] = room; // Connects the exit direction to the room
}
