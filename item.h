#pragma once 
// Ensures that the header file is included only once

#include <iostream> // Input/output stream library
#include <string> // String manipulation library

// Declaration of the Item class

class Item {
private:
    std::string name; // Holds the name of an item
    std::string description; // Holds the description of an item
public:
    Item(std::string name, std::string desc); // Constructor for the Item class
    void Interact(); // Allows interaction with the item
    std::string GetName(); // Retrieves the name of an item
    std::string GetDescription(); // Retrieves the description of an item
    bool operator==(Item& other); // Overloaded equality operator for comparing items
};
