#pragma once 
// Ensures that the header file is only included once

#include <iostream> // Input/output stream library
#include <string> // String manipulation library
#include <vector> // Provides functionality for dynamic arrays (vectors)
#include "Item.h" // Includes the header file for "Item.h"
#include "Room.h" // Includes the header file for "Room.h"


// Declaration of Character class and Player class

/*
    The Character class represents a character in the game, with features like hit points and experience.
*/

class Character {
private:
    std::string name; // Name of the character
    int health; // Health points of the character
    std::vector<Item> inventory; // Vector containing the character's inventory of items
public:
    Character(std::string& name, int health); // Constructor for the Character class
    void PrintStats(); // Prints the character's stats (health and inventory)
    void TakeDamage(int damage); // Decreases character's health
    void IncreaseHealth(int healthgain); // Increases character's health
    void AddToInventory(Item& newItem); // Adds an item to the character's inventory
    std::string& GetName(); // Returns the name of the character
    std::vector<Item>& GetInventory(); // Returns the character's inventory
};

/*
    The Player class inherits from the Character class and represents the player in the game.
*/

class Player : public Character {
private:
    Room* location; // Pointer pointing to the room where the player is located
public:
    Player(std::string name, int health); // Constructor for the Player class
    Room* GetLocation(); // Returns the location of the player
    void SetLocation(Room* newLocation); // Sets the new location for the player
};
